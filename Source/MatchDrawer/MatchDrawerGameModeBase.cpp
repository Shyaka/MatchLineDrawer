// Copyright Epic Games, Inc. All Rights Reserved.


#include "MatchDrawerGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "FieldWidget.h"
#include "MatchWidget.h"
#include "BlockWidget.h"

AMatchDrawerGameModeBase::AMatchDrawerGameModeBase()
{
	MaxRow = 7;
	MaxCol = 6;
	Score = 0;
	MaxMoves = 10;
}

void AMatchDrawerGameModeBase::StartMatch()
{
	// Initialize all the fields
	MatchUI = CreateWidget<UMatchWidget>(UGameplayStatics::GetPlayerController(this, 0), MatchWidget);
	MatchUI->AddToViewport();
	for (int i = 0; i < MaxCol; i++)
	{
		for (int j = 0; j < MaxRow; j++)
		{
			UFieldWidget* WidgetObj = CreateWidget<UFieldWidget>(UGameplayStatics::GetPlayerController(this, 0), FieldWidget);
			WidgetObj->FieldIndex = FIndexLoc(j, i);
			Fields.Add(WidgetObj);
		}
	}
	OnMatchStarted();
	MatchUI->PopulateFields(Fields);

	// initialize all the blocks

	for (UFieldWidget*& Field : Fields)
	{
		UBlockWidget* BlockInst = CreateWidget<UBlockWidget>(UGameplayStatics::GetPlayerController(this, 0), BlockWidget);
		BlockInst->BlockIndex = Field->FieldIndex;
		Blocks.Add(BlockInst);
		Field->PopulateBlock(BlockInst);
	}
}

void AMatchDrawerGameModeBase::RestartMatch()
{
	UGameplayStatics::OpenLevel(this, FName("L_Match"));
}

bool AMatchDrawerGameModeBase::Evaluate(int32& NumberOfBlocks)
{
	if (CurrentMoves >= MaxMoves)
	{
		//EndGame();
		return false;
	}

	//if (bGameEnded) return false;

	NumberOfBlocks = ActiveBlocks.Num();
	CurrentMoves++;
	bBackwardDetected = false;

	int32 EvaluateID;
	if (NumberOfBlocks >= 3) // only evaluate once we selected at least 3 blocks
	{
		EvaluateID = ActiveBlocks[0]->BlockID; // we check the type from the first selected block
	}
	else // end the evaluation if we did'nt select enough blocks
	{
		for (auto& ActiveBlock : ActiveBlocks)
		{
			GetFieldWidgetByIndex(ActiveBlock->BlockIndex)->OnStateChanged(false);
			ActiveBlock->bIsActivated = false;
			ActiveBlock->bNewBlock = false;
		}
		bStartedRecording = false;
		ActiveBlocks.Empty();
		return false;
	}

	// do checks to see if we selected blocks of the same type
	for (UBlockWidget*& Block : ActiveBlocks)
	{
		if (Block->BlockID == EvaluateID)
			continue;
		else
		{
			for (auto& ActiveBlock : ActiveBlocks)
			{
				GetFieldWidgetByIndex(ActiveBlock->BlockIndex)->OnStateChanged(false);
				ActiveBlock->bIsActivated = false;
				ActiveBlock->bNewBlock = false;
			}
			bStartedRecording = false;
			ActiveBlocks.Empty();
			return false;
		}
	}

	Score += (NumberOfBlocks * 5); // add score to each block by 5

	for (auto& Block : ActiveBlocks)
	{
		GetFieldWidgetByIndex(Block->BlockIndex)->OnStateChanged(false);
		GetFieldWidgetByIndex(Block->BlockIndex)->RemoveBlock(Block);
	}

	SpawnAndUpdateEmptyFields();
	bStartedRecording = false;
	ActiveBlocks.Empty();
	return true;
}

void AMatchDrawerGameModeBase::SetStartRecord(bool bRecord)
{
	bStartedRecording = bRecord;
}

void AMatchDrawerGameModeBase::AddBlockToActivatedBlocks(class UBlockWidget* BlockObject, FOnBackwardDetected BackWardsDelegate)
{
	if (CurrentMoves >= MaxMoves)
	{
		OnGameEnded.Broadcast(); // end the game once we hit the max moves
		return;
	}

	if (!BlockObject->bIsActivated) // checks to see if this block was not activated before
	{
		BlockObject->bIsActivated = true;

		GetFieldWidgetByIndex(BlockObject->BlockIndex)->OnStateChanged(true);

		ActiveBlocks.AddUnique(BlockObject);
		for (int i = 0; i < ActiveBlocks.Num() - 1; i++)
		{
			ActiveBlocks[i]->bNewBlock = false;
		}
	}
	else if (!BlockObject->bNewBlock) // here we detect that the player did a reverse play
	{
		for (auto& Block : ActiveBlocks)
		{
			GetFieldWidgetByIndex(Block->BlockIndex)->OnStateChanged(false);
			Block->bIsActivated = false;
			Block->bNewBlock = false;
		}
		BackWardsDelegate.Execute(BlockObject);
		bBackwardDetected = true;
		bStartedRecording = false;
		ActiveBlocks.Empty();
		CurrentMoves++;
	}
}

void AMatchDrawerGameModeBase::SpawnAndUpdateEmptyFields()
{
	// we check from the bottom of the first column to the top and we move to the next column after
	for (int i = 0; i < MaxCol; i++)
	{
		for (int j = MaxRow - 1; j >= 0; j--)
		{
			UBlockWidget* CurrentBlock = GetBlockWidgetByIndex(FIndexLoc(j, i));
			if (CurrentBlock == nullptr && j > 0)
			{
				int32 movedunits = 1; // the units we moved when searching for empty fields
				for (int x = j - 1; x >= 0; x--) // starts from the block above us
				{
					UBlockWidget* AboveBlock = GetBlockWidgetByIndex(FIndexLoc(x, i));

					if (AboveBlock != nullptr) // here we change the block position to the new location
					{
						UFieldWidget* DesiredField = GetFieldWidgetByIndex(FIndexLoc(x + movedunits, i)); // this is the location we're going to add our block

						if (DesiredField)
						{
							DesiredField->UpdateField(AboveBlock); // we move the block to the new position
						}
					}
					else
						movedunits++;
				}
			}
		}
	}

	// Spawn new blocks on empty fields
	for (int i = 0; i < MaxCol; i++)
	{
		for (int j = 0; j < MaxRow; j++)
		{
			UBlockWidget* CurrentBlock = GetBlockWidgetByIndex(FIndexLoc(j, i));
			if (CurrentBlock == nullptr) // we've reached to the end of the row index then spawn a random block
			{
				UBlockWidget* BlockInst = CreateWidget<UBlockWidget>(UGameplayStatics::GetPlayerController(this, 0), BlockWidget);
				BlockInst->BlockIndex = GetFieldWidgetByIndex(FIndexLoc(j, i))->FieldIndex;
				Blocks.Add(BlockInst);
				GetFieldWidgetByIndex(FIndexLoc(j, i))->PopulateBlock(BlockInst);
			}
		}
	}
}

class UBlockWidget* AMatchDrawerGameModeBase::GetBlockWidgetByIndex(FIndexLoc IndexLocation)
{
	UBlockWidget* Block = nullptr;

	for (UBlockWidget*& InBlock : Blocks)
	{
		if (InBlock->BlockIndex == IndexLocation) // checks to see if they have the same index
		{
			Block = InBlock;
			break;
		}
		else
			continue;
	}

	return Block;
}

class UFieldWidget* AMatchDrawerGameModeBase::GetFieldWidgetByIndex(FIndexLoc IndexLocation)
{
	UFieldWidget* Field = nullptr;

	for (UFieldWidget*& InField : Fields)
	{
		if (InField->FieldIndex == IndexLocation) // checks to see if they have the same index
		{
			Field = InField;
			break;
		}
		else
			continue;
	}

	return Field;
}

void AMatchDrawerGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	StartMatch();
}
