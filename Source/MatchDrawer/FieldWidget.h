// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MatchDrawerGameModeBase.h"
#include "FieldWidget.generated.h"

/**
 * 
 */
UCLASS()
class MATCHDRAWER_API UFieldWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly)
	FIndexLoc FieldIndex;

	UPROPERTY(BlueprintReadOnly)
	class UBlockWidget* CurrentBlock;

public:

	/** this populate the block to this field object */
	UFUNCTION(BlueprintNativeEvent)
	void PopulateBlock(class UBlockWidget* BlockWidget);

	/** this updates the block to this field */
	UFUNCTION(BlueprintCallable)
	void UpdateField(class UBlockWidget* NewBlockWidget);

	/** this clear the block attached to this field */
	UFUNCTION(BlueprintNativeEvent)
	void RemoveBlock(class UBlockWidget* BlockWidget);

	/** this gates called once the state of this field change */
	UFUNCTION(BlueprintImplementableEvent)
	void OnStateChanged(bool bActive);
	
};
