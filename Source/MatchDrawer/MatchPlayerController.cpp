// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchPlayerController.h"

void AMatchPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);

	bShowMouseCursor = true;
	FInputModeUIOnly InputMode;
	SetInputMode(InputMode);
}
