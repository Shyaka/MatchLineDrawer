// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MatchDrawerGameModeBase.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnBackwardDetected, class UBlockWidget*, Object);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameEnded);

USTRUCT(BlueprintType)
struct FIndexLoc
{
	GENERATED_BODY();

public:

	UPROPERTY(BlueprintReadOnly)
	int32 Row;

	UPROPERTY(BlueprintReadOnly)
	int32 Col;

	FIndexLoc(int32 InRow, int32 InCol) 
		: Row(InRow), Col(InCol)
	{}

	FIndexLoc()
	{
		Row = 0;
		Col = 0;
	}

	const bool operator== (const FIndexLoc& OtherIndex) const
	{
		return Row == OtherIndex.Row && Col == OtherIndex.Col;
	}
};

/**
 * 
 */
UCLASS()
class MATCHDRAWER_API AMatchDrawerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	AMatchDrawerGameModeBase();

	/** Maximum number of rows */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 MaxRow;

	/** Maximum number of columns */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 MaxCol;

	/** current score */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Score;

	/** amount of moves we can make before we end the game */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 MaxMoves;

	/** cuyrrent moves we already performed */
	UPROPERTY(BlueprintReadOnly)
	int32 CurrentMoves;

	/** array of all fields available */
	UPROPERTY(BlueprintReadOnly)
	TArray<class UFieldWidget*> Fields;

	/** current available blocks */
	UPROPERTY(BlueprintReadOnly)
	TArray<class UBlockWidget*> Blocks;

	/** current activated blocks */
	UPROPERTY(BlueprintReadOnly)
	TArray<class UBlockWidget*> ActiveBlocks;

	/** Match widget object */
	UPROPERTY(BlueprintReadOnly)
	class UMatchWidget* MatchUI;

	/** determines if we did a reverse move */
	UPROPERTY(BlueprintReadOnly)
	bool bBackwardDetected = false;

	/** determines if we started the move and we're capturing data */
	UPROPERTY(BlueprintReadOnly)
	bool bStartedRecording = false;

	/** this delegate gets called once the game ends */
	UPROPERTY(BlueprintAssignable)
	FOnGameEnded OnGameEnded;

protected:

	/** our field widget subclass */
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<class UFieldWidget> FieldWidget;

	/** our match widget subclass */
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<class UMatchWidget> MatchWidget;

	/** our block widget subclass */
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<class UBlockWidget> BlockWidget;

public:

	/** Starts the match */
	UFUNCTION(BlueprintCallable)
	void StartMatch();

	/** Restarts the match */
	UFUNCTION(BlueprintCallable)
	void RestartMatch();

	/** called once we started the match */
	UFUNCTION(BlueprintImplementableEvent)
	void OnMatchStarted();

	/** Does calculation and see if the latest move approves for score or not */
	UFUNCTION(BlueprintCallable)
	bool Evaluate(int32& NumberOfBlocks);

	/** sets that we're capturing data */
	UFUNCTION(BlueprintCallable)
	void SetStartRecord(bool bRecord);

	/** adds BlockObject to Activated objects */
	UFUNCTION(BlueprintCallable)
	void AddBlockToActivatedBlocks(class UBlockWidget* BlockObject, FOnBackwardDetected BackWardsDelegate);

	/** this will update all the blocks and move them to the empty fields below them and spawns new blocks to the new fields */
	UFUNCTION(BlueprintCallable)
	void SpawnAndUpdateEmptyFields();

	/** returns the block widget at a specified location */
	UFUNCTION(BlueprintPure)
	class UBlockWidget* GetBlockWidgetByIndex(FIndexLoc IndexLocation);

	/** returns the fielsd widget at a specified location */
	UFUNCTION(BlueprintPure)
	class UFieldWidget* GetFieldWidgetByIndex(FIndexLoc IndexLocation);

protected:

	virtual void BeginPlay() override;
	
};
