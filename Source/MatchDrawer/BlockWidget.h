// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MatchDrawerGameModeBase.h"
#include "BlockWidget.generated.h"

/**
 * 
 */
UCLASS()
class MATCHDRAWER_API UBlockWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	/** the index location of this block  */
	UPROPERTY(BlueprintReadOnly)
	FIndexLoc BlockIndex;

	/** the ID used to differentiate blocks */
	UPROPERTY(BlueprintReadOnly)
	int32 BlockID = 0;

	/** determines if this block was activated by the player */
	UPROPERTY(BlueprintReadOnly)
	bool bIsActivated = false;

	/** determines if this block is new and was'nt selected before */
	UPROPERTY(BlueprintReadOnly)
	bool bNewBlock = true;
	
};
