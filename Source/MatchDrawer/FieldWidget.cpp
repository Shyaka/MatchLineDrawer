// Fill out your copyright notice in the Description page of Project Settings.


#include "FieldWidget.h"
#include "BlockWidget.h"
#include "Kismet/GameplayStatics.h"

void UFieldWidget::PopulateBlock_Implementation(class UBlockWidget* BlockWidget)
{
	CurrentBlock = BlockWidget;

	if (CurrentBlock->BlockID == 0)
	{
		int32 Rand = FMath::RandRange(1, 4);
		CurrentBlock->BlockID = Rand;
	}
}

void UFieldWidget::UpdateField(class UBlockWidget* NewBlockWidget)
{
	NewBlockWidget->BlockIndex = FieldIndex;
	PopulateBlock(NewBlockWidget);
}

void UFieldWidget::RemoveBlock_Implementation(class UBlockWidget* BlockWidget)
{
	if (AMatchDrawerGameModeBase* GM = Cast<AMatchDrawerGameModeBase>(UGameplayStatics::GetGameMode(this)))
	{
		GM->Blocks.Remove(BlockWidget);
	}
}
