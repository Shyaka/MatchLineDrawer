// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MatchWidget.generated.h"

/**
 * 
 */
UCLASS()
class MATCHDRAWER_API UMatchWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/**
	 * initializes and populates fields to this widget
	 */
	UFUNCTION(BlueprintNativeEvent)
	void PopulateFields(const TArray<UFieldWidget*>& AllFields);
};
