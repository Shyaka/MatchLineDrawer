// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MatchPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MATCHDRAWER_API AMatchPlayerController : public APlayerController
{
	GENERATED_BODY()
	

public:

	virtual void OnPossess(APawn* aPawn) override;
};
